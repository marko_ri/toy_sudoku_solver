use std::collections::{HashMap, HashSet};

const DIGITS: [u8; 9] = [b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9'];
const ROWS: [u8; 9] = [b'A', b'B', b'C', b'D', b'E', b'F', b'G', b'H', b'I'];

type Square = [u8; 2];
type Candidates = Vec<u8>;

pub struct Board {
    squares: Vec<Square>,
    units: HashMap<Square, Vec<Vec<Square>>>,
    peers: HashMap<Square, HashSet<Square>>,
}

impl Board {
    pub fn new() -> Self {
        let squares = squares();
        let units = units(&squares);
        let peers = peers(&squares, &units);
        Self {
            squares,
            units,
            peers,
        }
    }
}

fn squares() -> Vec<Square> {
    cross(&ROWS, &DIGITS)
}

fn cross(rows: &[u8], digits: &[u8]) -> Vec<Square> {
    rows.iter().fold(Vec::new(), |mut acc, row| {
        for digit in digits.iter() {
            acc.push([*row, *digit]);
        }
        acc
    })
}

fn unit_list() -> Vec<Vec<Square>> {
    let mut result = Vec::new();

    for digit in DIGITS.into_iter() {
        result.push(cross(&ROWS, &[digit]));
    }
    for row in ROWS.into_iter() {
        result.push(cross(&[row], &DIGITS));
    }
    for rows in [[b'A', b'B', b'C'], [b'D', b'E', b'F'], [b'G', b'H', b'I']].iter() {
        for digits in [[b'1', b'2', b'3'], [b'4', b'5', b'6'], [b'7', b'8', b'9']].iter() {
            result.push(cross(rows, digits));
        }
    }

    result
}

fn units(squares: &[Square]) -> HashMap<Square, Vec<Vec<Square>>> {
    let mut result = HashMap::new();
    let unit_list = unit_list();

    for square in squares.iter() {
        for unit in unit_list.iter().filter(|u| u.contains(square)) {
            let v = result.entry(*square).or_insert(Vec::new());
            v.push(unit.clone());
        }
    }

    result
}

fn peers(
    squares: &[Square],
    units: &HashMap<Square, Vec<Vec<Square>>>,
) -> HashMap<Square, HashSet<Square>> {
    let mut result = HashMap::new();

    for square in squares.iter() {
        let mut set: HashSet<Square> = units
            .get(square)
            .unwrap()
            .iter()
            .flat_map(|v| v.iter().cloned())
            .collect();
        set.remove(square);
        result.insert(*square, set);
    }

    result
}

#[derive(Clone)]
pub struct Grid<'a> {
    candidates: HashMap<Square, Candidates>,
    board: &'a Board,
}

impl<'a> Grid<'a> {
    pub fn new(board: &'a Board) -> Self {
        // To start, every square can be any digit
        let mut candidates = HashMap::new();
        for square in board.squares.iter() {
            candidates.insert(*square, DIGITS.to_vec());
        }

        Self { candidates, board }
    }

    pub fn init(&mut self, start_state: &str) {
        assert_eq!(81, start_state.len());
        // Empty slots are marked as '0' or '.'
        let start_state: Vec<(Square, u8)> = self
            .board
            .squares
            .iter()
            .map(|sq| *sq)
            .zip(start_state.bytes())
            .collect();

        // assign values from start_state
        for (square, slot) in start_state.iter() {
            if DIGITS.iter().any(|d| d == slot) && !self.assign(square, *slot) {
                panic!("Start state has contradictions");
            }
        }
    }

    fn candidates(&self, square: &Square) -> &Candidates {
        self.candidates.get(square).unwrap()
    }

    fn candidates_mut(&mut self, square: &Square) -> &mut Candidates {
        self.candidates.get_mut(square).unwrap()
    }

    fn assign(&mut self, square: &Square, digit: u8) -> bool {
        let candidates = self.candidates(square).clone();
        for candidate in candidates.into_iter().filter(|&c| c != digit) {
            // eliminate all the values from square 'square' except 'digit'
            if !self.eliminate(square, candidate) {
                return false;
            }
        }

        true
    }

    fn eliminate(&mut self, square: &Square, digit: u8) -> bool {
        // if already eliminated
        if !self.candidates(square).contains(&digit) {
            return true;
        }

        // now is eliminated
        self.candidates_mut(square)
            .retain(|&candidate| candidate != digit);
        let remaining_candidates = self.candidates(square);
        if remaining_candidates.is_empty() {
            // Contradiction: removed last value
            return false;
        } else if remaining_candidates.len() == 1 {
            let last_candidate = remaining_candidates[0];
            // if a square has only one possible value, then eliminate
            // that value from the square's peers
            for peer in self.board.peers.get(square).unwrap().iter() {
                if !self.eliminate(peer, last_candidate) {
                    return false;
                }
            }
        }

        // If a unit is reduced to only one place for a digit, then put the digit there.
        for unit in self.board.units.get(square).unwrap().iter() {
            let places: Vec<&Square> = unit
                .iter()
                .filter(|&s| self.candidates(s).contains(&digit))
                .collect();
            if places.len() == 0 {
                // contradiction: no place for this value
                return false;
            } else if places.len() == 1 {
                // digit can only be in one place in unit; assign it there
                if !self.assign(places[0], digit) {
                    return false;
                }
            }
        }

        true
    }

    pub fn solve(&mut self) {
        self.candidates = self.search().unwrap_or_default();
    }

    // Using depth-first search and propagation, try all possible values.
    fn search(&self) -> Option<HashMap<Square, Candidates>> {
        if self
            .board
            .squares
            .iter()
            .map(|s| self.candidates(s))
            .all(|c| c.len() == 1)
        {
            // Solved
            return Some(self.candidates.clone());
        }

        // Chose the unfilled square s with the fewest possibilities
        let (mut min_square, mut min_candidates) = (&[b'0', b'0'], 10);
        for square in self.board.squares.iter() {
            if self.candidates(square).len() > 1 && self.candidates(square).len() < min_candidates {
                min_square = square;
                min_candidates = self.candidates(square).len();
            }
        }

        for candidate in self.candidates(&min_square).iter() {
            let mut self_cloned = self.clone();
            if self_cloned.assign(min_square, *candidate) {
                if let Some(result) = self_cloned.search() {
                    return Some(result);
                }
            }
        }

        None
    }
}

impl<'a> std::fmt::Display for Grid<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.candidates.is_empty() {
            return write!(f, "Nothing to see here");
        }
        let mut sorted_keys: Vec<&Square> = self.candidates.keys().collect();
        sorted_keys.sort();
        let sep = "------+------+------+\n";
        let mut vals = String::from(sep);
        for (i, key) in sorted_keys.into_iter().enumerate() {
            // vals.push_str(&String::from_utf8_lossy(self.candidates(key)).to_string());
            if self.candidates(key).len() == 1 {
                vals.push(self.candidates(key)[0].into());
            } else {
                vals.push('X');
            }

            vals.push(' ');
            if (i + 1) % 3 == 0 {
                vals.push('|');
            }
            if (i + 1) % 9 == 0 {
                vals.push('\n');
            }
            if (i + 1) % 27 == 0 {
                vals.push_str(sep);
            }
        }
        write!(f, "{}", vals)
    }
}

fn main() {
    let board = Board::new();

    let easy50_1 =
        "..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..";
    let easy50_2 =
        "2...8.3...6..7..84.3.5..2.9...1.54.8.........4.27.6...3.1..7.4.72..4..6...4.1...3";
    let top95_1 =
        "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......";
    let top95_2 =
        "52...6.........7.13...........4..8..6......5...........418.........3..2...87.....";
    let hardest_1 =
        "85...24..72......9..4.........1.7..23.5...9...4...........8..7..17..........36.4.";
    let hardest_2 =
        "..53.....8......2..7..1.5..4....53...1..7...6..32...8..6.5....9..4....3......97..";

    for start in [easy50_1, easy50_2, top95_1, top95_2, hardest_1, hardest_2] {
        let mut grid = Grid::new(&board);
        grid.init(start);
        grid.solve();
        println!("{}", grid);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cross() {
        let expected = vec![
            [b'A', b'1'],
            [b'A', b'2'],
            [b'A', b'3'],
            [b'B', b'1'],
            [b'B', b'2'],
            [b'B', b'3'],
            [b'C', b'1'],
            [b'C', b'2'],
            [b'C', b'3'],
        ];
        assert_eq!(expected, cross(&[b'A', b'B', b'C'], &[b'1', b'2', b'3']));
    }

    #[test]
    fn test_board() {
        let board = Board::new();
        assert_eq!(81, board.squares.len());
        assert_eq!(27, unit_list().len());

        for square in board.squares.iter() {
            assert_eq!(3, board.units.get(square).unwrap().len());
            assert_eq!(20, board.peers.get(square).unwrap().len());
        }

        let expected_units = vec![
            vec![
                [b'A', b'2'],
                [b'B', b'2'],
                [b'C', b'2'],
                [b'D', b'2'],
                [b'E', b'2'],
                [b'F', b'2'],
                [b'G', b'2'],
                [b'H', b'2'],
                [b'I', b'2'],
            ],
            vec![
                [b'C', b'1'],
                [b'C', b'2'],
                [b'C', b'3'],
                [b'C', b'4'],
                [b'C', b'5'],
                [b'C', b'6'],
                [b'C', b'7'],
                [b'C', b'8'],
                [b'C', b'9'],
            ],
            vec![
                [b'A', b'1'],
                [b'A', b'2'],
                [b'A', b'3'],
                [b'B', b'1'],
                [b'B', b'2'],
                [b'B', b'3'],
                [b'C', b'1'],
                [b'C', b'2'],
                [b'C', b'3'],
            ],
        ];
        assert_eq!(&expected_units, board.units.get(&[b'C', b'2']).unwrap());

        let expected_peers = HashSet::from([
            [b'A', b'2'],
            [b'B', b'2'],
            [b'D', b'2'],
            [b'E', b'2'],
            [b'F', b'2'],
            [b'G', b'2'],
            [b'H', b'2'],
            [b'I', b'2'],
            [b'C', b'1'],
            [b'C', b'3'],
            [b'C', b'4'],
            [b'C', b'5'],
            [b'C', b'6'],
            [b'C', b'7'],
            [b'C', b'8'],
            [b'C', b'9'],
            [b'A', b'1'],
            [b'A', b'3'],
            [b'B', b'1'],
            [b'B', b'3'],
        ]);
        assert_eq!(&expected_peers, board.peers.get(&[b'C', b'2']).unwrap());
    }
}
