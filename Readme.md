
Toy sudoku solver in Rust, based on the article: [Solve Every Sudoku Puzzle](https://norvig.com/sudoku.html) (faster solutions can be found on the net)